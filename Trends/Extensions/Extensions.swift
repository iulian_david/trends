//
//  Extensions.swift
//  Trends
//
//  Created by iulian david on 08/02/2020.
//  Copyright © 2020 iulian david. All rights reserved.
//

import Foundation

extension Double {
    func metersToKms(places: Int) -> String {
        let distanceFormatter = NumberFormatter()
        distanceFormatter.usesGroupingSeparator = true
        distanceFormatter.numberStyle = .decimal
        // localize to your grouping and decimal separator
        distanceFormatter.locale = Locale.current
        distanceFormatter.maximumFractionDigits = places
        distanceFormatter.minimumFractionDigits = places
        return distanceFormatter.string(from: NSNumber(value: self / 1000)) ?? "0.00"
    }
}


let formatterDuration: DateComponentsFormatter = {
    let formatter = DateComponentsFormatter()
    formatter.unitsStyle = .positional // Use the appropriate positioning for the current locale
    formatter.allowedUnits = [.hour, .minute, .second] // Units to display in the formatted string
    formatter.zeroFormattingBehavior = [.pad] // Pad with zeroes where appropriate for the locale
    return formatter
}()
