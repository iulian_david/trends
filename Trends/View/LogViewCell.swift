//
//  LogViewCell.swift
//  Trends
//
//  Created by iulian david on 05/01/2020.
//  Copyright © 2020 iulian david. All rights reserved.
//

import UIKit

class LogViewCell: UITableViewCell {


    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var paceLabel: UILabel!
    @IBOutlet weak var dateOfRunLabel: UILabel!
    @IBOutlet weak var paceUnit: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func config(with log: LogData) {
        durationLabel.text = "\(log.minutesRun)"
        distanceLabel.text = "\(log.kms)"
        paceUnit.text = "/km"
        dateOfRunLabel.text = log.strDateOfRun
        paceLabel.text = formatterDuration.string(from: TimeInterval(log.pace))
    }

}
