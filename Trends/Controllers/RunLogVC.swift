//
//  RunLogVC.swift
//  Trends
//
//  Created by iulian david on 05/01/2020.
//  Copyright © 2020 iulian david. All rights reserved.
//

import UIKit

class RunLogVC: UIViewController {

    @IBOutlet weak var logTableView: UITableView!

    
    private var dataManager = DataManager()
    private lazy var dataSourceProvider = LogViewDatasource(dataManager: dataManager)


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dataManager.reloadData()
        if logTableView != nil {
            logTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
        logTableView.delegate = dataSourceProvider
        logTableView.dataSource = dataSourceProvider
        
    }
}



