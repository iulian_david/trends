//
//  RunningVC.swift
//  Trends
//
//  Created by iulian david on 08/01/2020.
//  Copyright © 2020 iulian david. All rights reserved.
//

import UIKit
import MapKit

class RunningVC: LocationVC {

    var buttonCenter: CGPoint = .zero

    @IBOutlet weak var swipeBGImageView: UIImageView!
    @IBOutlet weak var sliderImageView: UIImageView!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var paceLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var pauseBtn: UIButton!

    var startLocation: CLLocation!
    var lastLocation: CLLocation!

    var runDistance = 0.0 {
        didSet {
            distanceLabel.text = "\(runDistance.metersToKms(places: 2))"
            if secondsCounter > 0 {
                paceLabel.text = "\(formatterDuration.string(from: TimeInterval(1000 * Double(secondsCounter) / runDistance)) ?? "00:00")"
            }
        }
    }

    var timer: Timer?

    var secondsCounter = 0 {
        didSet {
            durationLabel.text = "\(formatterDuration.string(from: TimeInterval(secondsCounter)) ?? "00:00")"
        }
    }
    
    var coordinateLocations = [Location]()

    override func viewWillAppear(_ animated: Bool) {
        manager?.delegate = self
        manager?.distanceFilter = 10
        startRun()
    }


    private func startRun() {
        coordinateLocations = [Location]()
        manager?.startUpdatingLocation()
        startTimer()
        pauseBtn.setImage(#imageLiteral(resourceName: "pauseButton"), for: .normal)
    }


    private func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }

    @objc private func updateCounter() {
        secondsCounter += 1
    }


    private func pauseRun() {
        startLocation = nil
        lastLocation = nil
        timer?.invalidate()
        manager?.stopUpdatingLocation()
        pauseBtn.setImage(#imageLiteral(resourceName: "resumeButton"), for: .normal)
    }

    private func endRun() {
        timer?.invalidate()
        manager?.stopUpdatingLocation()
        Run.addRunToRealm(pace: Int(1000 * Double(secondsCounter) / runDistance), distance: runDistance, duration: secondsCounter, locations: coordinateLocations)
    }

    override func viewDidLoad() {
        super.viewDidLoad()


        let swipeGesture = UIPanGestureRecognizer(target: self, action: #selector(endRunSwiped(sender:)))
        sliderImageView.addGestureRecognizer(swipeGesture)
        sliderImageView.isUserInteractionEnabled = true

        swipeGesture.delegate = self as? UIGestureRecognizerDelegate
        runDistance = 0.0
        secondsCounter = 0
    }

    @IBAction func pauseBtnPressed(_ sender: Any) {
        if timer?.isValid ?? false {
            pauseRun()
        } else {
            startRun()
        }
    }

    @objc func endRunSwiped(sender: UIPanGestureRecognizer) {
        let minAdjust: CGFloat = 80
        let maxAdjust: CGFloat = 130

        if let sliderView = sender.view {
            if sender.state == .began || sender.state == .changed {
                let translation = sender.translation(in: self.view)
                //NO change for y axis
                if sliderImageView.center.x >= (swipeBGImageView.center.x - minAdjust)
                           && sliderImageView.center.x <= (swipeBGImageView.center.x + maxAdjust) {
                    sliderView.center = CGPoint(x: sliderView.center.x + translation.x, y: sliderView.center.y)
                } else if sliderImageView.center.x >= (swipeBGImageView.center.x + maxAdjust) {
                    sliderImageView.center.x
                            = swipeBGImageView.center.x + maxAdjust
                    //END RUN
                    endRun()
                    dismiss(animated: true)
                } else {
                    sliderImageView.center.x = swipeBGImageView.center.x - minAdjust
                }

                sender.setTranslation(CGPoint.zero, in: view)
            } else if sender.state == .ended {
                UIView.animate(withDuration: 0.1, delay: 0.1, options: [.curveEaseIn], animations: { [unowned self] in
                    sliderView.center.x = self.swipeBGImageView.center.x - minAdjust
                }, completion: nil)
            }
        }

    }
}

extension RunningVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            checkLocationAuthStatus()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if startLocation == nil {
            startLocation = locations.first
            lastLocation = locations.first
        } else if let location = locations.last {
            runDistance += lastLocation.distance(from: location)
        }
        coordinateLocations.insert(Location(latitude: lastLocation.coordinate.latitude, longitude: lastLocation.coordinate.longitude), at: 0)
        lastLocation = locations.last
    }
}
