//
//  BeginRunVC.swift
//  Trends
//
//  Created by iulian david on 05/01/2020.
//  Copyright © 2020 iulian david. All rights reserved.
//

import UIKit
import MapKit
import RealmSwift

class BeginRunVC: LocationVC {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var lastRunCloseBtn: UIButton!
    @IBOutlet weak var paceLbl: UILabel!
    @IBOutlet weak var DistanceLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var lastRunView: UIView!
    @IBOutlet weak var labelStack: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkLocationAuthStatus()
       
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        manager?.delegate = self
        manager?.startUpdatingLocation()
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        manager?.stopUpdatingLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        mapView.delegate = self
        setupMapView()
    }

    fileprivate func hideLastRun() {
        lastRunView.alpha = 0
        lastRunCloseBtn.alpha = 0
        labelStack.isHidden = true
        centerMapOnUserLocation()
    }
    
    @IBAction func closeLastRunView(_ sender: Any) {
        hideLastRun()
    }
    
    @IBAction func runTapped(_ sender: Any) {
        guard let runningVC = storyboard?.instantiateViewController(withIdentifier: "RunningVC") else {
            fatalError("Could not load RunningVC")
        }
        runningVC.modalPresentationStyle = .fullScreen
        present(runningVC, animated: true)
    }

    @IBAction func locationCenterPressed(_ sender: Any) {
        centerMapOnUserLocation()
    }
    
    
    func centerMapOnUserLocation() {
        mapView.userTrackingMode = .follow
        let coordinateRegion = MKCoordinateRegion(center: mapView.userLocation.coordinate, latitudinalMeters: 500, longitudinalMeters: 500)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func centerMapOnPreviousRoute(locations: List<Location>) -> MKCoordinateRegion {
        guard let initialLocation = locations.first else {
            return MKCoordinateRegion()
        }
        var minLat = initialLocation.latitude
        var minLong = initialLocation.longitude
        var maxLat = minLat
        var maxLong = minLong
        locations.forEach { (location) in
            minLat = min(minLat,location.latitude)
            maxLat = max(maxLat, location.latitude)
            minLong = min(minLong,location.longitude)
            maxLong = max(maxLong, location.longitude)
        }
        return MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: (minLat + maxLat)/2, longitude: (minLong + maxLong)/2), span: MKCoordinateSpan(latitudeDelta: (maxLat - minLat) * 1.4, longitudeDelta: (maxLong - minLong) * 1.4))
    }
    
    private func setupMapView() {
        guard let lastRun = Run.getAll().first else {
            hideLastRun()
            return
        }
        
        lastRunView.alpha = 0.45
        lastRunCloseBtn.alpha = 1
        labelStack.isHidden = false
        let log = LogData(duration: lastRun.duration, distance: lastRun.distance, dateOfRun: lastRun.date, pace: lastRun.pace)
        durationLbl.text = "\(log.minutesRun)"
        DistanceLbl.text = "\(log.kms)"
        paceLbl.text = formatterDuration.string(from: TimeInterval(log.pace))
        
        if !mapView.overlays.isEmpty {
            mapView.removeOverlays(mapView.overlays)
        }

        let coordinates: [CLLocationCoordinate2D] = lastRun.locations.map {
            CLLocationCoordinate2D(latitude: $0.latitude, longitude: $0.longitude)
        }
        
        mapView.userTrackingMode = .none
        mapView.setRegion(centerMapOnPreviousRoute(locations: lastRun.locations), animated: true)
        mapView.addOverlay(MKPolyline(coordinates: coordinates, count: lastRun.locations.count))
    }

}

extension BeginRunVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            checkLocationAuthStatus()
            mapView.showsUserLocation = true
//            mapView.userTrackingMode = .follow
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        guard let poliLyne = overlay as? MKPolyline else {
            return MKOverlayRenderer(overlay: overlay)
        }
        let renderer = MKPolylineRenderer(polyline: poliLyne)
        renderer.strokeColor = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
        renderer.lineWidth = 4
        return renderer
    }
}
