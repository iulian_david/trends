//
//  File.swift
//  Trends
//
//  Created by iulian david on 05/01/2020.
//  Copyright © 2020 iulian david. All rights reserved.
//

import Foundation

struct LogData {
    let duration: Int
    let distance: Double
    let dateOfRun: Date
    let pace: Int

    var strDateOfRun: String {
        return dateformatter.string(from: dateOfRun)
    }

    var kms: String {
        return "\(String(format: "%0.*f", 2, distance / 1000)) km"
    }

    var minutesRun: String {
        guard let dur = TimeInterval(exactly: duration) else {
            return "00:00:00"
        }
        return formatterDuration.string(from: dur) ?? "00:00:00"
    }
}
