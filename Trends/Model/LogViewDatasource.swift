//
//  LogViewDatasource.swift
//  Trends
//
//  Created by iulian david on 05/01/2020.
//  Copyright © 2020 iulian david. All rights reserved.
//

import UIKit

class LogViewDatasource: NSObject, UITableViewDataSource, UITableViewDelegate {

    private let dataManager: DataManager

    init(dataManager: DataManager) {
        self.dataManager = dataManager
        super.init()
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataManager.itemsCount
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LogViewCell", for: indexPath) as! LogViewCell
        let item = dataManager.item(at: indexPath.row)
        cell.config(with: item)
        return cell
    }

    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            dataManager.delete(at: indexPath.row)
            tableView.reloadData()
        }
    }


}
