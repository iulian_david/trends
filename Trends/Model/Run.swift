//
// Created by iulian david on 08/02/2020.
// Copyright (c) 2020 iulian david. All rights reserved.
//

import RealmSwift

class Run: Object {
    @objc dynamic public private(set) var id = ""
    @objc dynamic public private(set) var pace = 0
    @objc dynamic public private(set) var distance = 0.0
    @objc dynamic public private(set) var duration = 0
    @objc dynamic public private(set) var date = Date()
    var locations = List<Location>()
    

    override class func primaryKey() -> String {
        "id"
    }

    override class func indexedProperties() -> [String] {
        ["pace", "date", "duration"]
    }

    convenience init(pace: Int, distance: Double, duration: Int, locations: List<Location>) {
        self.init()
        self.id = UUID().uuidString.lowercased()
        self.date = Date()
        self.pace = pace
        self.distance = distance
        self.duration = duration
        self.locations = locations
    }

    static func addRunToRealm(pace: Int, distance: Double, duration: Int, locations: [Location]) {
        REALM_QUEUE.sync {
            do {
                let realm = try Realm()
                try realm.write {
                    //realm.add(locations)
                    let realmLocations = List<Location>()
                    realmLocations.append(objectsIn: locations)
                    realm.add(Run(pace: pace, distance: distance, duration: duration, locations: realmLocations))
                    try realm.commitWrite()
                }
            } catch {
                debugPrint("Error adding to Realm")
            }
        }

    }
    
    static func getAll() -> [Run] {
        var runs = [Run]()
        REALM_QUEUE.sync {
            do {
                let realm = try Realm()
                let allRuns: Results<Run> = realm.objects(Run.self).sorted(byKeyPath: "date", ascending: false)
                runs.append(contentsOf: allRuns)
            } catch {
                debugPrint("Error getting Run array")
            }
        }
        return runs
    }
}
