//
//  DataManager.swift
//  Trends
//
//  Created by iulian david on 05/01/2020.
//  Copyright © 2020 iulian david. All rights reserved.
//

import Foundation

class DataManager {
    
    public func reloadData() {
       items = Run.getAll().map { (run) -> LogData in
        return LogData(duration: run.duration, distance: run.distance, dateOfRun: run.date, pace: run.pace)
        }
        print("reloadData()")
    }
    
    private(set) var items = [LogData]()
    
    public var itemsCount: Int {
        return items.count
    }

    public func item(at index: Int) -> LogData {
        return items[index]
    }

    public func add(item: LogData) {
        items.append(item)
    }

    public func delete(at index: Int) {
        items.remove(at: index)
    }

    public func edit(item: LogData, at index: Int) {
        items[index] = item
    }

}


private extension Date {
    init(_ dateString: String) {
        let date = dateformatter.date(from: dateString)!
        self.init(timeInterval: 0, since: date)
    }
}


let dateformatter: DateFormatter = {
    let dateStringFormatter = DateFormatter()
    dateStringFormatter.dateFormat = "yyyy-MM-dd"
    dateStringFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
    return dateStringFormatter
}()



